<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Listado de Usuario')
        <small>@yield('contentheader_description')</small>
    </h1>
    
</section>